'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TickerSchema = new Schema({
    open: Number,
    close: Number,
    time: String,
    pair: String,
    sma: Number
});

module.exports = mongoose.model('Ticker', TickerSchema);