const axios = require('axios');
const clear = require('clear');
const chalks = require('chalk');
const mongoose = require('mongoose');
const Ticker = require('./models/ticker_model');
const schedule = require('node-schedule');
const math = require('mathjs');
const fs = require('fs');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const telegram_api_token = "786041644:AAFvf7iu8jHBLHgk0BF16ymFVPz7nSVJqjE";
const TelegramBot = require('node-telegram-bot-api');
const bot = new TelegramBot(telegram_api_token, { polling: true });

var coins = [{
    name: 'btc_idr',
    previous_price: null
}, {
    name: 'trx_idr',
    previous_price: null
}, {
    name: 'eth_idr',
    previous_price: null
}, {
    name: 'str_idr',
    previous_price: null
}, {
    name: 'xrp_idr',
    previous_price: null
}, {
    name: 'bchsv_idr',
    previous_price: null
}, {
    name: 'waves_idr',
    previous_price: null
}, {
    name: 'gsc_idr',
    previous_price: null
}, {
    name: 'ada_idr',
    previous_price: null
}];

mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://codetorium:n3v3rth3s4m3@mongodb.miniwebalerting.svc:27017/botalerting`);
schedule.scheduleJob({ minute: [0, 15, 30, 45] }, function () {
    clear();
    bot.sendMessage(270076566, "Tick update");
    var date = new Date();
    var requestList = [];
    coins.forEach((value) => {
        requestList.push(axios.get(`https://indodax.com/api/${value.name}/ticker`));
    });

    axios.all(requestList)
        .then(response => {
            response
                .forEach((value, index) => {
                    Ticker.findOneAndUpdate({ "time": date.getFormatted, "pair": coins[index].name },
                        {
                            "close": coins[index].previous_price || null,
                            "time": date.getTime(),
                            "open": value.data.ticker.last,
                            "pair": coins[index].name
                        },
                        { upsert: true }, (err) => {
                            if (err) console.log(`ERROR ${err}`);
                        }
                    );
                    coins[index].previous_price = value.data.ticker.last;
                });
        })
        .catch(error => {
            console.log(error);
        });

    coins.forEach(coin => {
        Ticker
            .find({ "pair": coin.name })
            .sort('-time')
            .exec((err, tickers) => {
                if (err) {
                    console.log("ERROR QUERYING THE DATA");
                }

                var period = 20
                var lastPeriodTicker = tickers.slice(1, period + 1)
                var hasNullClose = lastPeriodTicker.find((m) => m.close == null)

                if (lastPeriodTicker.length == period && !hasNullClose) {
                    var lastPeriodTickerClose = lastPeriodTicker.map(value => value.close)

                    var sum = 0;
                    lastPeriodTickerClose.forEach(value => {
                        sum += value;
                    })
                    var sma = lastPeriodTicker.length == period ? sum / period : null;

                    var bollingerFactor = 2;
                    var upperBollinger = sma + (math.std(lastPeriodTickerClose) * bollingerFactor);
                    var lowerBollinger = sma - (math.std(lastPeriodTickerClose) * bollingerFactor);
                }

                var lastTicker = tickers[0] || null
                if (lastTicker != null) {
                    var chalkOpt;
                    if (lowerBollinger > lastTicker.close) {
                        chalkOpt = chalks.bgGreen.black;
                        fs.appendFileSync("new_alert_text", "Buy " + lastTicker.pair + " Price : " + lastTicker.close + " Time : " + lastTicker.time + "\n");
                        bot.sendMessage(270076566, "Buy " + lastTicker.pair + " Price : " + lastTicker.close + " Time : " + lastTicker.time + "\n"
                            + "LB : " + lowerBollinger + " " + lastTicker.pair + " UB : " + upperBollinger);
                    } if (upperBollinger < lastTicker.close) {
                        chalkOpt = chalks.bgRed.black;
                        fs.appendFileSync("new_alert_text", "Sell " + lastTicker.pair + " Price : " + lastTicker.close + " Time : " + lastTicker.time + "\n");
                        bot.sendMessage(270076566, "Sell " + lastTicker.pair + " Price : " + lastTicker.close + " Time : " + lastTicker.time + "\n"
                            + "LB : " + lowerBollinger + " " + lastTicker.pair + " UB : " + upperBollinger);
                    } else {
                        chalkOpt = chalks.bgWhite.black;
                        //bot.sendMessage(270076566, "Neutral " + lastTicker.pair + " Price : " + lastTicker.close + " Time : " + lastTicker.time);
                    }
                    console.log(chalkOpt((lastTicker.pair)) + "open " + lastTicker.open + " close " + lastTicker.close);
                }
            });
    })
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.listen(process.env.PORT || 2991);
